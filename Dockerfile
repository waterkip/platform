FROM registry.gitlab.com/zaaksysteem/bttw-tools:v0.008

RUN cpanm LWP::Protocol::https \
 && cpanm https://gitlab.com/zaaksysteem/syzygy.git@v0.004 \
 && cpanm https://gitlab.com/zaaksysteem/zaaksysteem-instance_config.git \
 && rm -rf /root/.cpanm \
 || (cat /root/.cpanm/work/*/build.log && false)

CMD ["/bin/bash"]
